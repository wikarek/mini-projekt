<?php
    
    function create_dat($conn){
        $sql_query  =  'CREATE DATABASE IF NOT EXISTS portal';
        if($conn->query($sql_query) === TRUE){
            echo "utworzono baze danych";
        }
        else{
            echo ("nie udalo sie utworzyc bazy danych \n". $conn->error);
        }
        $conn->close();
        header('Location: /panel_adm.php');
        exit();
    }
    
    function delete_dat($conn){
        $sql_query  =  'DROP DATABASE IF EXISTS portal';
        if($conn->query($sql_query) === TRUE){
            echo "usunieto tabele  portal";
        }
        else{
            echo ("nie udalo sie usunac tabeli portal ". $conn->error);
        }
        $conn->close();
        header('Location: /panel_adm.php');
        exit();
    }

    function create_tab($conn){
        $sql_query  = "
        CREATE TABLE IF NOT EXISTS strony(
            id INT(7) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            link varchar(50) not null ,
            typ  varchar(20) not null,
            etykieta varchar(20) not null
        )";
        if($conn->query($sql_query) === TRUE){
            echo "Utworzono tabele strony";
        }
        else{
            echo ("nie udalo sie utworzyc tabeli strony ". $conn->error);
        }
        $conn->close();
        header('Location: /panel_adm.php');
        exit();
    }

    function delete_tab($conn){
        $sql_query  =  "DROP TABLE IF EXISTS strony";
        if($conn->query($sql_query) === TRUE){
            echo "usunieto tabele  strony";
        }
        else{
            echo ("nie udalo sie usunac tabeli strony ". $conn->error);
        }
        $conn->close();
        header('Location: /panel_adm.php');
        exit();
    }
    
    function insert_link($conn){
        $link = (string)$conn->real_escape_string($_POST['user_link']);
        $typ_link = $conn->real_escape_string($_POST['link']);
        $etykieta = $conn->real_escape_string($_POST['etykieta']);
        if ($link === ""){
            echo "pole link jest  puste";
        }
        elseif($typ_link === ""){
            echo "pole typ_link jest puste";
        }
        else{
            if($typ_link === "link_zewnetrzny"){
                if((strstr($link,"http://")===FALSE)&&(strstr($link,"https://")===FALSE)){
                    $tmp="http://";
                    $tmp.=$link;
                    $link=$tmp;
                }
            }
            $sql_query = 'INSERT INTO strony (link, typ, etykieta) Values (?, ?, ?)';
            $stm = $conn->prepare($sql_query);
            $stm->bind_param('sss', $link, $typ_link, $etykieta);
            if ($stm->execute() === false) {
                echo('Rekord nie został dodany'. $conn->error);
            }
            else{
                echo "Dodano nowy rekord do tabeli";
                header('Location: /panel_adm.php');
            }
            $conn->close();
            
        }
        header('Location: /panel_adm.php');
        exit();
    }

    function show_links_panel($conn){
        $sql_query = 'SELECT id, link, typ, etykieta FROM strony';
        $st = $conn->query($sql_query);
        $ilosc_rek = 0;
        if ($st !== false){
            $ilosc_rek = $st->num_rows;
        }
        $rekordy = [];
        if ($ilosc_rek > 0){
            $rekordy = $st->fetch_all(MYSQLI_ASSOC);
        }
        return ['rekordy' => $rekordy];
    }

    function delete_link($conn){
        $id = (int)$conn->real_escape_string($_GET['del_rek']);
        if (empty($id) || !is_numeric($id)) {
            die('Rekord nie został usunięty, id rekordu nie udalo sie przesłac poprawnie');
        }
        $sql_query = " DELETE FROM strony WHERE id = ?";
        $stm = $conn->prepare($sql_query);
        $stm->bind_param('i', $id);
        if ($stm->execute() === false){
            die("zapytanie sie nie wykonalo");
        }else{
            echo "rekord zostal usuniety";
        }
        header('Location: /panel_adm.php');
        exit();
    }

    function pozyskaj_rekord($conn, int $id){
        $sql_query = "SELECT * FROM strony WHERE id = ?";
        $stm = $conn->prepare($sql_query);
        $stm->bind_param('i', $id);
        if($stm->execute() === false){
            die("zapytanie sie nie wykonalo");
        }
        else{
            return $stm->get_result()->fetch_array();
        }
        header('Location: /panel_adm.php');
        exit();
    }

    function rename_link($conn){
        if (!isset($_POST['edytowanie_rekordow']['submit'])) {
            die('Formularz został przesłany nieprawidłowo');
        }
        $form = $_POST['edytowanie_rekordow'];
        foreach ($form as $field => $value) {
            if (is_null($value)) {
                die("Pole '{$field}' nie może być puste!");
            }
        }
        $link = $form['user_link'];
        $typ_link = $form['link'];
        $id = $form['id'];
        $etykieta= $form['etykieta'];
        
        if($typ_link === "link_zewnetrzny"){
                if((strstr($link,"http://")===FALSE)&&(strstr($link,"https://")===FALSE)){
                    $tmp="http://";
                    $tmp.=$link;
                    $link=$tmp;
                }
            }
        if (empty($id) || !is_numeric($id)) {
            die('Rekord nie został zaktualizowany z powodu id ');   
        }
        $sql_query = "
        UPDATE strony SET link = ?, typ = ?, etykieta = ?
        WHERE id = ?       
        ";
        $stm = $conn->prepare($sql_query);
        $stm->bind_param('sssi',  $link, $typ_link,$etykieta, $id);
        if ($stm->execute() === false){
            die("zapytanie sie nie wykonalo");
        }else{
            echo "dodano rekord";
        }
        header('Location: /panel_adm.php');
        exit();
    }

    function show_links_users($conn){
        $sql_query = 'SELECT link, etykieta FROM strony';
        $st = $conn->query($sql_query);
        $ilosc_rek = 0;
        if ($st !== false){
            $ilosc_rek = $st->num_rows;
        }
        $rekordy = [];
        if ($ilosc_rek > 0){
            $rekordy = $st->fetch_all(MYSQLI_ASSOC);
        }
        return ['rekordy' => $rekordy];
    }
?>