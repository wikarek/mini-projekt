<?php
    session_name('auth');
    session_set_cookie_params (3600);
    session_start();
    if (isset($_SESSION['zalogowany'])){
       include_once 'operation_database.php';
        $database = include 'config_database.php';
        $host_database = $database['host'];
        $name_database = $database['database'];
        $user_database = $database['user'];
        $pass_database = $database['password'];
        $conn = new mysqli($host_database,$user_database,$pass_database);
        if($conn -> connect_error){
            echo "Nie udalo sie polaczyc z baza " . $conn -> connect_error;
        }
        if(isset($_GET['add_dat'])){
            create_dat($conn);
        }
        if(isset($_GET['del_dat'])){
            $conn = new mysqli($host_database,$user_database,$pass_database, $name_database);
            if($conn->connect_error){
                die("nie udalo sie polaczyc ". $conn->connect_error);
            }
            delete_dat($conn);
        }
        if(isset($_GET['create_tab'])){
            $conn = new mysqli($host_database,$user_database,$pass_database, $name_database);
            if($conn->connect_error){
                die("nie udalo sie polaczyc ". $conn->connect_error);
            }
            create_tab($conn);
        }
        if(isset($_GET['del_tab'])){
            $conn = new mysqli($host_database,$user_database,$pass_database, $name_database);
            if($conn->connect_error){
                die("nie udalo sie polaczyc ". $conn->connect_error);
            }
            delete_tab($conn);
        }
        if (isset($_GET['ins_link'])){
            $conn = new mysqli($host_database,$user_database,$pass_database, $name_database);
            if($conn->connect_error){
                die("nie udalo sie polaczyc ". $conn->connect_error);
            }
            insert_link($conn);
        }

        if(isset($_GET['del_rek'])  && !empty($_GET['del_rek'])){
            $conn = new mysqli($host_database,$user_database,$pass_database, $name_database);
            if($conn->connect_error){
                die("nie udalo sie polaczyc ". $conn->connect_error);
            }
            delete_link($conn);
            $conn->close();
        }
        if(isset($_GET['update_rekord']) && !empty($_GET['update_rekord']) && is_numeric($_GET['update_rekord']) ){
            $conn = new mysqli($host_database,$user_database,$pass_database, $name_database);
            if($conn->connect_error){
                die("nie udalo sie polaczyc ". $conn->connect_error);
            }
            $rekord = pozyskaj_rekord($conn,  $_GET['update_rekord']);
        }

        if (empty($_GET) && isset($_POST['edytowanie_rekordow']) && !empty($_POST['edytowanie_rekordow'])) {
            $conn = new mysqli($host_database,$user_database,$pass_database, $name_database);
            if($conn->connect_error){
                die("nie udalo sie polaczyc ". $conn->connect_error);
            }
            rename_link($conn);
        }
        if(isset($_GET['logout'])){
            $conn->close();
            session_destroy();
            header('Location: /logowanie.php');
        }
    }
    
    else {
         header('Location: /logowanie.php');
         exit();
    }

    include('naglowek.php');
?>            
<form action="/panel_adm.php/?ins_link" method="POST">
    <table class="tabela_adm">
    <tr>
    <th>Typ linku</th>
    <th>Link</th>
    <th>Etykieta</th>
    <th></th>
    </tr>
    <tr><td>
    <select id="link_type" name="link">
        <option value=link_wewnetrzny>Link wewnetrzny</option>
        <option value=link_zewnetrzny>Link zewnetrzny</option>
    </select>
    </td>
    <td>
    <input type="text" name="user_link" id="user_link"/>
    </td>
    <td>
    <input type="text" name="etykieta" id="etykieta"/>
    <td>
    <input type="submit" value="dodaj link" />
    </td></tr>
    </table>
</form>
</div>
<?php
    include('stopka.php'); 
?>
               
            