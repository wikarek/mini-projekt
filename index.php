<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="utf-8"/>
        <title>Mini portal lab5</title>
        <meta name="author" content="Mateusz jablonski & Marcin Jalocha">
        <meta name="description" content="mini portal">
        <link rel="stylesheet" href="style.css"/>
        <meta http-equiv="X-UA-Compatible" content="IE=7" />
    </head>
    <body>
        <div class="portal">
            <div class="menu">
                <?php
                    include_once 'operation_database.php';
                    $database = include 'config_database.php';
                    $host_database = $database['host'];
                    $name_database = $database['database'];
                    $user_database = $database['user'];
                    $pass_database = $database['password'];
                    error_reporting(0);
                    try{
                                $conn = new mysqli($host_database,$user_database,$pass_database, $name_database);
                                if($conn->connect_error){
                                    
                                    throw new Exception("Nie utworzono bazy danych",50);
                           }
                           }
                           catch(Exception $e){
                                die("Wystapil blad ladowania strony");
                           }
                    $zawartosc_tabeli = show_links_users($conn); ?> 
                   <?php if(empty($zawartosc_tabeli['rekordy'])){
                                echo "tabela jest pusta";
                           }
                           else{?>
                              <?php foreach($zawartosc_tabeli['rekordy'] as $rekordy):?>
                                    
                                            <button onclick='wyswietl_link("<?=$rekordy['link']; ?>")'><?=$rekordy['etykieta']; ?></button>                                    
                                <?php endforeach; } ?>     
            </div>
            <div class="content">   
                <script>
                    function wyswietl_link(link){
                        var ifr = document.getElementById("ramka");
                        ifr.setAttribute("src",link);
                    }
                </script>
                <iframe id="ramka"></iframe>
            </div>
        </div>
    </body>
</html>