<?php
    $user_data =  include_once 'user.php'; 
    $error_log = false;
    session_set_cookie_params (20);
    session_cache_limiter('nocache');
    session_name('auth');
    session_start();
    if(!isset($_SESSION['licznik'])){
        $_SESSION['licznik']=0;
    }
    if($_SESSION['licznik']>3){
        session_unset();
        header('Location: /authlim.php');
    }
    if(isset($_POST['przycisk_log'])){
            $username = htmlspecialchars((string)$_POST['username']);
            $password = htmlspecialchars((string)$_POST['password']);
            if( $user_data['login'] === $username  and $user_data['password'] === $password ){
                
                if(!isset($_SESSION['zalogowany']))
                {
                    $_SESSION['zalogowany'] = true;
                }
                header('Location: /panel_adm.php');
            }
            else{
                $error_log = true;
                $_SESSION['licznik']++;
            }
    }
    else{
	$_SESSION['licznik']++;
    }

?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="utf-8"/>
        <title>Mini portal lab5</title>
        <meta name="author" content="Mateusz jablonski & Marcin Jalocha">
        <meta name="description" content="mini portal">
        <link rel="stylesheet" href="style.css"/>
    </head>
    <body>
        <div class="okno_logowania">
            <form action="logowanie.php" method="POST">
                    <label for="username">Nazwa użytkownika:</label></br>
                    <input type="text" name="username"/></br>
                    <label for="password">Hasło:</label></br>
                    <input type="password" name="password"/></br>
                    <input type="submit" value="Zaloguj się" name="przycisk_log" class="przycisk_log"/>
                    <?php
                        if($error_log === true){
                            ?>
                            <p class="error_m">nie prawidlowy login i haslo</p>
                           <?php
                        }
                    ?>
            </form>
        </div>
    </body>
</html>

